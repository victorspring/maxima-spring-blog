package life.maxima.spring.service;

import life.maxima.spring.dto.CommentDto;
import life.maxima.spring.dto.PostDto;
import life.maxima.spring.entity.Comment;
import life.maxima.spring.entity.Post;
import life.maxima.spring.entity.Tag;
import life.maxima.spring.entity.User;
import life.maxima.spring.repository.CommentRepository;
import life.maxima.spring.repository.PostRepository;
import life.maxima.spring.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.time.LocalDateTime;
import java.util.Set;

import static life.maxima.spring.utils.SecurityUtils.*;

@Service
@Transactional
public class CommentServiceImpl implements CommentService {

    private final PostRepository postRepository;
    private final UserRepository userRepository;
    private final CommentRepository commentRepository;

    @Autowired
    public CommentServiceImpl(PostRepository postRepository,
                              UserRepository userRepository,
                              CommentRepository commentRepository) {
        this.postRepository = postRepository;
        this.userRepository = userRepository;
        this.commentRepository = commentRepository;
    }

    @Override
    public void create(Long postId, String content) {
        Comment comment = new Comment();
        comment.setContent(content);
        comment.setDtCreated(LocalDateTime.now());

        Post post = postRepository.findById(postId).orElseThrow();
        comment.setPost(post);

        User user = userRepository.findByUsername(
                getCurrentUserDetails().getUsername()).orElseThrow();
        comment.setUser(user);
        commentRepository.save(comment);
    }

    @Override
    public Comment findById(Long id) {
        return commentRepository.findById(id).orElseThrow();
    }

    @Override
    @Secured("ROLE_USER")
    public Comment create(CommentDto commentDto) {
        Comment comment = new Comment();
        if (StringUtils.hasText(commentDto.getContent())) {
            comment.setContent(commentDto.getContent());
        }
        UserDetails details = getCurrentUserDetails();
        comment.setDtCreated(LocalDateTime.now());
        comment.setUser(userRepository
                .findByUsername(details.getUsername())
                .orElseThrow());

        comment.setPost(postRepository
                .findById(commentDto.getPostId())
                .orElseThrow());

        return commentRepository.save(comment);
    }

    @Secured("ROLE_USER")
    @Override
    public Comment update(Long commentId, CommentDto commentDto) {
        Comment comment = commentRepository.findById(commentId).orElseThrow();
        checkAuthorityOnComment(comment);

        if (StringUtils.hasText(commentDto.getContent())) {
            comment.setContent(commentDto.getContent());
        }


        comment.setDtUpdated(LocalDateTime.now());
        return commentRepository.save(comment);

    }

    @Override
    public void delete(long commentId) {
        Comment comment = commentRepository.findById(commentId).orElseThrow();
        checkAuthorityOnComment(comment);
        commentRepository.deleteById(commentId);
    }







}
