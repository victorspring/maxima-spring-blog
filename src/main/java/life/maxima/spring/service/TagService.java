package life.maxima.spring.service;

import life.maxima.spring.entity.Tag;

import java.util.List;

public interface TagService {

    void create(String name);

    void create(String... names);

    Tag findByName(String tagName);

    List<Tag> findAll();

}
