package life.maxima.spring.service;

import life.maxima.spring.dto.PostDto;
import life.maxima.spring.entity.Post;

import java.util.List;
import java.util.Optional;

public interface PostService {
    Post create(PostDto postDto);

    Post findById(long postId);

    Post update(long postId, PostDto postDto);

    void delete(long postId);

    List<Post> findAll(Optional<String> query);

}
