package life.maxima.spring.controller.api;

import life.maxima.spring.dto.CommentDto;
import life.maxima.spring.dto.PostDto;
import life.maxima.spring.repository.CommentRepository;
import life.maxima.spring.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

import static life.maxima.spring.utils.Sorts.SORT_BY_DT;
import static org.springframework.http.ResponseEntity.noContent;
import static org.springframework.http.ResponseEntity.ok;

@RestController
@RequestMapping("/api/comment")
public class CommentApiController {

    private final CommentRepository commentRepository;
    private final CommentService commentService;


    @Autowired
    public CommentApiController(CommentRepository commentRepository,
                                CommentService commentService) {
        this.commentRepository = commentRepository;
        this.commentService = commentService;
    }

    @GetMapping
    public ResponseEntity<List<CommentDto>> findAll(){
        return ok(commentRepository.findAll(SORT_BY_DT)
                .stream()
                .map(CommentDto::fromComment)
                .collect(Collectors.toList()));
    }

    @GetMapping("/{commentId}")
    public ResponseEntity<CommentDto> findById(@PathVariable Long commentId){
        return ResponseEntity.ok(CommentDto.fromComment(commentService.findById(commentId)));
    }

    @PostMapping
    public ResponseEntity<CommentDto> create(@RequestBody CommentDto commentDto){
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(CommentDto.fromComment(commentService.create(commentDto)));
    }

    @PutMapping("/{commentId}")
    public ResponseEntity<CommentDto> update(@PathVariable long commentId,
                                             @RequestBody CommentDto commentDto){
        return ResponseEntity.ok(CommentDto.fromComment(commentService.update(commentId, commentDto)));
    }

    @DeleteMapping("/{postId}")
    public ResponseEntity<Void> delete(@PathVariable long postId){
        commentService.delete(postId);
        return noContent().build();
    }


}

