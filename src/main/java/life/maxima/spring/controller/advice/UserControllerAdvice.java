package life.maxima.spring.controller.advice;

import life.maxima.spring.controller.UserController;
import life.maxima.spring.repository.UserRepository;
import life.maxima.spring.service.TagService;
import life.maxima.spring.service.UserService;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.persistence.EntityExistsException;

@ControllerAdvice(assignableTypes = UserController.class)
public class UserControllerAdvice {

    @ExceptionHandler(EntityExistsException.class)
    public String handleEntityExistsException(Model model){
        model.addAttribute("error", "Username already exists");
        return "redirect:/sign-up";
    }


}
