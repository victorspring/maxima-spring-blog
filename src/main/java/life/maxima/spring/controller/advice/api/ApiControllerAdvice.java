package life.maxima.spring.controller.advice.api;

import life.maxima.spring.controller.api.CommentApiController;
import life.maxima.spring.controller.api.PostApiController;
import life.maxima.spring.dto.ErrorMessageDto;
import org.springframework.beans.TypeMismatchException;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.server.MethodNotAllowedException;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.NoSuchElementException;

@Order(Ordered.HIGHEST_PRECEDENCE)
@RestControllerAdvice(assignableTypes = {PostApiController.class, CommentApiController.class})
public class ApiControllerAdvice extends ResponseEntityExceptionHandler {

    @ExceptionHandler(NoSuchElementException.class)
    public ResponseEntity<ErrorMessageDto> handleNotFound(){
        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .body(new ErrorMessageDto(HttpStatus.NOT_FOUND.value(), "Not found"));
    }

    @ExceptionHandler(AccessDeniedException.class)
    public ResponseEntity<ErrorMessageDto> handleAccessDenied(AccessDeniedException e){
        return ResponseEntity.status(HttpStatus.FORBIDDEN)
                .body(new ErrorMessageDto(HttpStatus.FORBIDDEN.value(), "Access Denied"));
    }

    @ExceptionHandler({BadCredentialsException.class,
            AuthenticationException.class})
    public ResponseEntity<ErrorMessageDto> handleBadCredentials(){
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED)
                .body(new ErrorMessageDto(HttpStatus.UNAUTHORIZED.value(), "Bad Credentials"));
    }


    @Override
    public ResponseEntity<Object> handleTypeMismatch(TypeMismatchException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(new ErrorMessageDto(HttpStatus.BAD_REQUEST.value(), "Bad Request"));
    }
}
