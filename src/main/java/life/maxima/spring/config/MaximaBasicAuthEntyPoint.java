package life.maxima.spring.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import life.maxima.spring.dto.ErrorMessageDto;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.www.BasicAuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class MaximaBasicAuthEntyPoint extends BasicAuthenticationEntryPoint {

    private final ObjectMapper mapper = new ObjectMapper();

    @Override
    public void commence(
            HttpServletRequest request,
            HttpServletResponse response,
            AuthenticationException authEx)
            throws IOException {
        response.addHeader("WWW-Authenticate", "Basic realm=" + getRealmName() + "");
        response.addHeader("Content-Type", "application/json");
        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        ErrorMessageDto dto = new ErrorMessageDto(
                HttpStatus.UNAUTHORIZED.value(), "Bad Credentials");

        PrintWriter writer = response.getWriter();
        writer.println(mapper.writeValueAsString(dto));
    }


    @Override
    public void afterPropertiesSet() {
        setRealmName("Maxima");
        super.afterPropertiesSet();
    }


}
