package life.maxima.spring.utils;

import life.maxima.spring.entity.Comment;
import life.maxima.spring.entity.Post;
import life.maxima.spring.entity.Role;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Objects;
import java.util.stream.Collectors;


public class SecurityUtils {

    private static final String ACCESS_DENIED = "Access Denied";

    public static UserDetails getCurrentUserDetails() {
        Object principal = SecurityContextHolder.getContext()
                .getAuthentication()
                .getPrincipal();

        if (principal instanceof UserDetails userDetails) {
            return userDetails;
        } else {
            throw new AccessDeniedException(ACCESS_DENIED);
        }
    }

    public static void checkAuthorityOnPost(Post post) {
        if (!isAuthor(post)) {
            throw new AccessDeniedException(ACCESS_DENIED);
        }
    }

    public static void checkIsAuthorOrAdmin(Post post) {
        UserDetails user = getCurrentUserDetails();
        if (!isAuthor(post) && !isAdmin()){
            throw new AccessDeniedException(ACCESS_DENIED);
        }
    }

    public static boolean isAuthor(Post post) {
        return isAuthor(getCurrentUserDetails(), post);
    }

    private static boolean isAuthor(UserDetails user, Post post) {
        return Objects.equals(user.getUsername(), post.getUser().getUsername());
    }

    private static boolean isAuthor(UserDetails user, Comment comment) {
        return Objects.equals(user.getUsername(), comment.getUser().getUsername());
    }

    public static boolean isAuthor(Comment comment) {
        return isAuthor(getCurrentUserDetails(), comment);
    }

    public static void checkAuthorityOnComment(Comment comment) {
        if (!isAuthor(comment)) {
            throw new AccessDeniedException(ACCESS_DENIED);
        }
    }


    public static boolean isAdmin() {
        return isAdmin(getCurrentUserDetails());
    }

    private static boolean isAdmin(UserDetails user) {
        return user.getAuthorities()
                .stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.toSet())
                .contains("ROLE_" + Role.ADMIN);
    }


}
