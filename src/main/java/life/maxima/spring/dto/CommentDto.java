package life.maxima.spring.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import life.maxima.spring.entity.Comment;
import life.maxima.spring.entity.Tag;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import java.time.LocalDateTime;
import java.util.stream.Collectors;

@Setter
@Getter
public class CommentDto {
    private Long id;
    private String content;
    private String author;

    @JsonProperty("post_id")
    private Long postId;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("dt_created")
    private LocalDateTime dtCreated;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("dt_updated")
    private LocalDateTime dtUpdated;

    public static CommentDto fromComment(Comment comment){
        CommentDto commentDto = new CommentDto();
        commentDto.setId(comment.getCommentId());
        commentDto.setContent(comment.getContent());
        commentDto.setAuthor(comment.getUser().getUsername());
        commentDto.setDtCreated(comment.getDtCreated());
        commentDto.setDtUpdated(comment.getDtUpdated());
        commentDto.setPostId(comment.getPost().getPostId());

        return commentDto;
    }


}

