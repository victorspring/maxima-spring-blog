package life.maxima.spring.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import life.maxima.spring.entity.Post;
import life.maxima.spring.entity.Tag;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import java.time.LocalDateTime;
import java.util.stream.Collectors;

@Setter
@Getter
public class PostDto {

    private Long id;
    private String title;
    private String content;

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private String tags;
    private String author;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("dt_created")
    private LocalDateTime dtCreated;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("dt_updated")
    private LocalDateTime dtUpdated;

    public static PostDto fromPost(Post post){
        PostDto postDto = new PostDto();
        postDto.setId(post.getPostId());
        postDto.setTitle(post.getTitle());
        postDto.setContent(post.getContent());
        postDto.setTags(post.getTags().stream()
                .map(Tag::getName)
                .sorted()
                .collect(Collectors.joining(" ")));
        postDto.setAuthor(post.getUser().getUsername());
        postDto.setDtCreated(post.getDtCreated());
        postDto.setDtUpdated(post.getDtUpdated());

        return postDto;
    }

}
