DROP TABLE IF EXISTS comment;
DROP TABLE IF EXISTS post_tag;
DROP TABLE IF EXISTS tag;
DROP TABLE IF EXISTS post;

DROP TABLE IF EXISTS user_role;
DROP TABLE IF EXISTS role;
DROP TABLE IF EXISTS "user";

--ACID

CREATE TABLE "user" (
    user_id BIGSERIAL PRIMARY KEY,
    username VARCHAR(50) NOT NULL UNIQUE,
    password VARCHAR(200) NOT NULL,
    dt_created TIMESTAMP NOT NULL,
    is_active BOOLEAN DEFAULT false
);

CREATE TABLE role(
    role_id BIGSERIAL PRIMARY KEY,
    name VARCHAR(50) NOT NULL
);

CREATE TABLE user_role (
    user_id BIGINT REFERENCES "user" (user_id) ON DELETE CASCADE NOT NULL,
    role_id BIGINT REFERENCES role(role_id) ON DELETE CASCADE NOT NULL,
    PRIMARY KEY (user_id, role_id)
);


CREATE TABLE post (
    post_id bigserial PRIMARY KEY,
    user_id bigint REFERENCES "user" (user_id) ON DELETE CASCADE NOT NULL,
    title varchar(100) NOT NULL,
    content text NOT NULL,
    dt_created timestamp NOT NULL,
    dt_updated timestamp
);

CREATE TABLE tag (
    tag_id bigserial PRIMARY KEY,
    name varchar(50) NOT NULL
);


CREATE TABLE post_tag (
    post_id bigint REFERENCES post(post_id) ON DELETE CASCADE NOT NULL,
    tag_id bigint REFERENCES tag(tag_id) ON DELETE CASCADE NOT NULL,
    PRIMARY KEY (post_id, tag_id)
);

CREATE TABLE comment (
    comment_id bigserial PRIMARY KEY,
    user_id bigint REFERENCES "user" (user_id) ON DELETE CASCADE NOT NULL,
    post_id bigint REFERENCES post(post_id) ON DELETE CASCADE NOT NULL,
    content text,
    dt_created timestamp NOT NULL,
    dt_updated timestamp
);




--Data

insert into role(name) values ('ADMIN');
insert into role(name) values ('USER');

insert into "user" (username, password, dt_created, is_active)
    values ('admin', '$2a$10$sHtGTZSYIDyTAOxy6/QS1ubN8gWR1nZV4hsE8CEAmoLnIu03C3Yj2', current_timestamp, true);

insert into "user" (username, password, dt_created, is_active)
    values ('user2', '$2a$10$urBgE.ao9n5xKNp/sC7Ej.AqsnIqoz3mcvf2nGJ5w9c1QLne.VIHq', current_timestamp, true);

insert into "user" (username, password, dt_created, is_active)
    values ('user3', '$2a$10$XgkR9tNdTmZmKHs9u8QL5.aEMB98YWyp9A1hXhp/DWyr.3waq.b2S', current_timestamp, true);

insert into user_role(user_id, role_id) values (1, 1);
insert into user_role(user_id, role_id) values (2, 2);
insert into user_role(user_id, role_id) values (3, 2);

insert into post (user_id, title, content, dt_created, dt_updated)
	values (2, 'Day 1', 'It''s all good!', to_timestamp('2022-05-14 18:05:50', 'yyyy-MM-dd HH24:mm:ss'), null);
insert into post (user_id, title, content, dt_created, dt_updated)
	values (2, 'Day 2', 'It''s all ok!', to_timestamp('2022-05-15 18:05:50', 'yyyy-MM-dd HH24:mm:ss'), null);
insert into post (user_id, title, content, dt_created, dt_updated)
	values (3, 'Day 3', 'It''s all bad!', to_timestamp('2022-05-16 18:05:50', 'yyyy-MM-dd HH24:mm:ss'), null);

insert into tag (name) values ('news');
insert into tag (name) values ('life');
insert into tag (name) values ('day');
insert into tag (name) values ('mood');
insert into tag (name) values ('ideas');
insert into tag (name) values ('thoughts');

insert into post_tag(post_id, tag_id) values (1, 1);
insert into post_tag(post_id, tag_id) values (1, 2);
insert into post_tag(post_id, tag_id) values (2, 3);
insert into post_tag(post_id, tag_id) values (2, 2);
insert into post_tag(post_id, tag_id) values (2, 1);
insert into post_tag(post_id, tag_id) values (2, 5);
insert into post_tag(post_id, tag_id) values (3, 3);
insert into post_tag(post_id, tag_id) values (3, 2);
insert into post_tag(post_id, tag_id) values (3, 6);

insert into comment(user_id, post_id, content, dt_created)
    values(2, 2, 'Nice!', to_timestamp('2022-05-14 18:05:50', 'yyyy-MM-dd HH24:mm:ss'));
insert into comment(user_id, post_id, content, dt_created)
    values(3, 1, 'Awesome!', to_timestamp('2022-05-15 18:05:50', 'yyyy-MM-dd HH24:mm:ss'));
insert into comment(user_id, post_id, content, dt_created)
    values(3, 1, 'Excellent!', to_timestamp('2022-05-16 18:05:50', 'yyyy-MM-dd HH24:mm:ss'));
insert into comment(user_id, post_id, content, dt_created)
    values(2, 1, 'Wonderful!', to_timestamp('2022-05-17 18:05:50', 'yyyy-MM-dd HH24:mm:ss'));
insert into comment(user_id, post_id, content, dt_created)
    values(3, 3, 'Disgusting!', to_timestamp('2022-05-18 18:05:50', 'yyyy-MM-dd HH24:mm:ss'));
insert into comment(user_id, post_id, content, dt_created)
    values(3, 3, 'Atrocious!', to_timestamp('2022-05-19 18:05:50', 'yyyy-MM-dd HH24:mm:ss'));

select * from comment;