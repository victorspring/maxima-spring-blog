import life.maxima.spring.config.DBConfig;
import life.maxima.spring.dto.CommentDto;
import life.maxima.spring.entity.Comment;
import life.maxima.spring.repository.CommentRepository;
import org.json.JSONException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.postgresql.shaded.com.ongres.scram.common.bouncycastle.base64.Base64;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.http.*;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestOperations;

import java.nio.charset.StandardCharsets;

import static life.maxima.spring.utils.FileUtils.asString;
import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = DBConfig.class)
@Sql(value = "classpath:blog.sql",
executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
public class CommentApiControllerTest {

    private static final String API_URL = "http://localhost:8080/maxima-spring-blog/api/comment";
    private static final String LOGIN = "user3";
    private static final String PASSWORD = "user3";

    private final RestOperations restOperations;
    private final CommentRepository commentRepository;

    @Value("classpath:testFindAllComment.json")
    private Resource testFindAllComment;

    @Value("classpath:testFindByIdComment.json")
    private Resource testFindByIdComment;

    @Autowired
    public CommentApiControllerTest(RestOperations restOperations, CommentRepository commentRepository) {
        this.restOperations = restOperations;
        this.commentRepository = commentRepository;
    }

    @Test
    void testFindAll() throws JSONException {
        ResponseEntity<String> res =
                restOperations.getForEntity(API_URL, String.class);

        assertEquals(HttpStatus.OK, res.getStatusCode());
        JSONAssert.assertEquals(asString(testFindAllComment), res.getBody(), true);
    }

    @Test
    void testFindByIdComment() throws JSONException {
        ResponseEntity<String> res =
                restOperations.getForEntity(API_URL + "/1", String.class);

        assertEquals(HttpStatus.OK, res.getStatusCode());
        JSONAssert.assertEquals(asString(testFindByIdComment), res.getBody(), true);
    }

    @Test
    void testCreate(){
        CommentDto commentDto = new CommentDto();
        commentDto.setContent("One more comment");
        commentDto.setPostId(2L);

        ResponseEntity<CommentDto> res =
                restOperations.postForEntity(API_URL,
                        new HttpEntity<>(commentDto, getHeaders()), CommentDto.class);

        assertEquals(HttpStatus.CREATED, res.getStatusCode());

        Comment comment = commentRepository.findById(res.getBody().getId()).orElseThrow();

        assertEquals("One more comment", comment.getContent());
        assertEquals(2L, comment.getPost().getPostId());

        assertEquals(7, commentRepository.count());
    }

    @Test
    void testUpdate(){
        CommentDto commentDto = new CommentDto();
        commentDto.setContent("One more comment");

        restOperations.put(API_URL + "/2",
                        new HttpEntity<>(commentDto, getHeaders()));

        Comment comment = commentRepository.findById(2L).orElseThrow();

        assertEquals("One more comment", comment.getContent());
        assertEquals(1L, comment.getPost().getPostId());
        assertEquals("user3", comment.getUser().getUsername());


        assertEquals(6, commentRepository.count());
    }

    @Test
    void delete(){
        ResponseEntity<Void> res = restOperations.exchange(API_URL + "/2",
                HttpMethod.DELETE,
                new HttpEntity<>(getHeaders()),
                Void.class);
        assertEquals(HttpStatus.NO_CONTENT, res.getStatusCode());
        assertEquals(5, commentRepository.count());
    }

    private MultiValueMap<String, String> getHeaders() {
        String auth = LOGIN + ":" + PASSWORD;
        byte[] encoded = Base64.encode(auth.getBytes(StandardCharsets.UTF_8));

        HttpHeaders headers = new HttpHeaders();
        headers.set(HttpHeaders.CONTENT_TYPE, "application/json");
        headers.set(HttpHeaders.AUTHORIZATION, "Basic " + new String(encoded));

        return headers;
    }

}