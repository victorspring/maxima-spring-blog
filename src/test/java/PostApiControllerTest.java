import life.maxima.spring.config.DBConfig;
import life.maxima.spring.dto.PostDto;
import life.maxima.spring.entity.Post;
import life.maxima.spring.entity.Tag;
import life.maxima.spring.repository.PostRepository;
import life.maxima.spring.repository.TagRepository;
import org.json.JSONException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.http.*;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.util.FileCopyUtils;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestOperations;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UncheckedIOException;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.List;

import static life.maxima.spring.utils.FileUtils.asString;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertIterableEquals;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = DBConfig.class)
@Sql(scripts = "classpath:blog.sql",
        executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
public class PostApiControllerTest {

    private static final String API_URL = "http://localhost:8080/maxima-spring-blog/api/post";
    private static final String LOGIN = "user2";
    private static final String PASSWORD = "user2";

    private final RestOperations restOperations;
    private final PostRepository postRepository;
    private final TagRepository tagRepository;

    @Value("classpath:testFindAll.json")
    private Resource testFindAll;

    @Value("classpath:testFindById.json")
    private Resource testFindById;

    @Autowired
    public PostApiControllerTest(RestOperations restOperations,
                                 PostRepository postRepository, TagRepository tagRepository) {
        this.restOperations = restOperations;
        this.postRepository = postRepository;
        this.tagRepository = tagRepository;
    }

    @Test
    void testFindAll() throws JSONException {
        ResponseEntity<String> res =
                restOperations.getForEntity(API_URL, String.class);
        assertEquals(HttpStatus.OK, res.getStatusCode());
        JSONAssert.assertEquals(asString(testFindAll), res.getBody(), true);
    }


    @Test
    void testFindById() throws JSONException {
        ResponseEntity<String> res =
                restOperations.getForEntity(API_URL + "/1", String.class);
        assertEquals(HttpStatus.OK, res.getStatusCode());
        JSONAssert.assertEquals(asString(testFindById), res.getBody(), true);
    }

    @Test
    void testCreate() {
        PostDto postDto = new PostDto();
        postDto.setTitle("Day 9");
        postDto.setContent("One more post");
        postDto.setTags("life news");

        ResponseEntity<PostDto> res =
                restOperations.postForEntity(API_URL,
                        new HttpEntity<>(postDto, getHeaders()), PostDto.class);
        assertEquals(HttpStatus.CREATED, res.getStatusCode());

        Post post = postRepository.findById(res.getBody().getId()).orElseThrow();
        assertEquals("Day 9", post.getTitle());
        assertEquals("One more post", post.getContent());
        List<Tag> tags = tagRepository.findByPosts_postId(res.getBody().getId());
        assertIterableEquals(List.of("life", "news"), tags.stream()
                .map(Tag::getName)
                .sorted()
                .toList());
        assertEquals(4, postRepository.count());

    }

    @Test
    void testUpdate() {
        PostDto postDto = new PostDto();
        postDto.setTitle("Day 9");
        postDto.setContent("One more post");
        postDto.setTags("life news");

        restOperations.put(API_URL + "/1", new HttpEntity<>(postDto, getHeaders()));

        Post post = postRepository.findById(1L).orElseThrow();
        assertEquals("Day 9", post.getTitle());
        assertEquals("One more post", post.getContent());
        List<Tag> tags = tagRepository.findByPosts_postId(1L);
        assertIterableEquals(List.of("life", "news"), tags.stream()
                .map(Tag::getName)
                .sorted()
                .toList());
        assertEquals(3, postRepository.count());
    }

    @Test
    void delete() {
        ResponseEntity<Void> res = restOperations.exchange(API_URL + "/1",
                HttpMethod.DELETE,
                new HttpEntity<>(getHeaders()),
                Void.class);
        assertEquals(HttpStatus.NO_CONTENT, res.getStatusCode());
        assertEquals(2, postRepository.count());
    }



    private MultiValueMap<String, String> getHeaders() {
        String auth = LOGIN + ":" + PASSWORD;
        byte[] encoded = Base64.getEncoder().encode(auth.getBytes(StandardCharsets.UTF_8));

        HttpHeaders headers = new HttpHeaders();
        headers.set(HttpHeaders.CONTENT_TYPE, "application/json");
        headers.set(HttpHeaders.AUTHORIZATION, "Basic " + new String(encoded));

        return headers;
    }

}
